from flask import Flask, render_template, url_for, redirect, request

from db_repo import shorten_url, retrieve_original_shortened_urls

app = Flask(__name__)


@app.route('/')
def index():
    '''
    Index page collecting urls to be shortened and display shortened urls and their original links
    :return: a view displaying shortened url and collecting more urls
    '''
    shortened_and_original_url_pairs = retrieve_original_shortened_urls()
    server_url = request.url

    # adding server url to shortened url for flexibility, because server url may change
    server_url_added = list(map(lambda x: {
        "id": server_url + str(x["id"]),
        "original_url": x["original_url"]
    },
                                shortened_and_original_url_pairs))

    return render_template('index.html', result=server_url_added)


@app.route('/generateKey', methods=['POST'])
def generate_key():
    '''
    Endpoint: collect url and save shortened url in database
    :return: redirect to index page
    '''
    original_url = request.form['original_url']
    shorten_url(original_url)
    return redirect(url_for('index'))


if __name__ == '__main__':
    app.run()
