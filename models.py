import os

from peewee import *

DATABASE = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'test.db')
sqlite_db = SqliteDatabase(DATABASE)


class BaseModel(Model):
    class Meta:
        database = sqlite_db


class Url(BaseModel):
    id = BigIntegerField(primary_key=True, unique=True)
    original_url = TextField()


# Create the tables if not exist
sqlite_db.create_tables([Url])
