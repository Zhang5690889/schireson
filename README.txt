Simple URL shortener

Supported python version: 3.7

Installation:
run
pip install -r requirements.txt


Start the application by running:
python app.py

Shortener Design:
I use database to generate unique id as shortened urls. Two benefits of doing this:
1. Database generates unique keys, so I do not have to worry about it.
2. Using database creates persistent storage.

Technology used:
1. Flask: basic web server
2. Peewee: lightweight sql orm
3. sqlite: lightweight portable file based SQL database