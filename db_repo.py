from models import *


def shorten_url(original_url):
    '''
    Saves original url to database and database generates unique id
    :param original_url: original url to be shortened
    :return:
    '''
    query = Url.select().where(Url.original_url == original_url)
    if not query.exists():
        record = Url(original_url=original_url)
        record.save()


def retrieve_original_shortened_urls():
    '''
    Retrieve shortened and original urls
    :return: shortened and original url pairs
    '''
    result = []
    for value in Url.select():
        result.append({
            "id": value.id,
            "original_url": value.original_url
        })
    return result
